package sbs.spring.weather.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import sbs.spring.weather.model.CountryWeather;
import sbs.spring.weather.service.WeatherService;

@Controller
@RequestMapping("/weather")
public class WeatherController {
	
	@Autowired
	WeatherService weatherService;

	@GetMapping("/oneCountry")
	public String getWeather(Model model) {
		System.out.println("getWeather called..");
		
		List<CountryWeather> weatherList = weatherService.getWeather();
		model.addAttribute("weatherList", weatherList);
		
		System.out.println("getTodayWeather Ended...");
		return "weatherList";
	}
	 
}
