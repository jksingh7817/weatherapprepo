package sbs.spring.weather.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class Datum {

@JsonProperty("time")
public int time;
@JsonProperty("precipIntensity")
public float precipIntensity;
@JsonProperty("precipIntensityError")
public float precipIntensityError;
@JsonProperty("precipProbability")
public float precipProbability;
@JsonProperty("precipType")
public String precipType;
}
