package sbs.spring.weather.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Setter
@Getter
@NoArgsConstructor
public class Currently {
	@JsonProperty("time")
	public int time;
	@JsonProperty("summary")
	public String summary;
	@JsonProperty("icon")
	public String icon;
	@JsonProperty("nearestStormDistance")
	public int nearestStormDistance;
	@JsonProperty("nearestStormBearing")
	public int nearestStormBearing;
	@JsonProperty("precipIntensity")
	public float precipIntensity;
	@JsonProperty("precipIntensityError")
	public float precipIntensityError;
	@JsonProperty("precipProbability")
	public float precipProbability;
	@JsonProperty("precipType")
	public String precipType;
	@JsonProperty("temperature")
	public float temperature;
	@JsonProperty("apparentTemperature")
	public float apparentTemperature;
	@JsonProperty("dewPoint")
	public float dewPoint;
	@JsonProperty("humidity")
	public float humidity;
	public float pressure;
	@JsonProperty("windSpeed")
	public float windSpeed;
	@JsonProperty("windGust")
	public float windGust;
	@JsonProperty("windBearing")
	public int windBearing;
	@JsonProperty("cloudCover")
	public float cloudCover;
	@JsonProperty("uvIndex")
	public int uvIndex;
	@JsonProperty("visibility")
	public int visibility;
	@JsonProperty("ozone")
	public float ozone;
}
