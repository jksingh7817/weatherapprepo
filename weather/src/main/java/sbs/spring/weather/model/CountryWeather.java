package sbs.spring.weather.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class CountryWeather {
	
	public float latitude;
	public float longitude;
	public String timezone;
	public Currently currently;
	public Minutely minutely;
	public Hourly hourly;
	public Daily daily;
	public Flags flags;
	public int offset;
}
