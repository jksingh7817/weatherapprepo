package sbs.spring.weather.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class Flags {
	@JsonProperty("sources")
	public List<String> sources = null;
	@JsonProperty("nearest-station")
	public float nearestStation;
	@JsonProperty("units")
	public String units;
}
