package sbs.spring.weather.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class Daily {
	@JsonProperty("summary")
	public String summary;
	@JsonProperty("icon")
	public String icon;
	@JsonProperty("data")
	public List<Datum__> data = null;
}
