package sbs.spring.weather.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class Datum__ {
	@JsonProperty("time")
	public int time;
	@JsonProperty("summary")
	public String summary;
	@JsonProperty("icon")
	public String icon;
	@JsonProperty("sunriseTime")
	public int sunriseTime;
	@JsonProperty("sunsetTime")
	public int sunsetTime;
	@JsonProperty("moonPhase")
	public float moonPhase;
	@JsonProperty("precipIntensity")
	public float precipIntensity;
	@JsonProperty("precipIntensityMax")
	public float precipIntensityMax;
	@JsonProperty("precipIntensityMaxTime")
	public int precipIntensityMaxTime;
	@JsonProperty("precipProbability")
	public float precipProbability;
	@JsonProperty("precipType")
	public String precipType;
	@JsonProperty("temperatureHigh")
	public float temperatureHigh;
	@JsonProperty("temperatureHighTime")
	public int temperatureHighTime;
	@JsonProperty("temperatureLow")
	public float temperatureLow;
	@JsonProperty("temperatureLowTime")
	public int temperatureLowTime;
	@JsonProperty("apparentTemperatureHigh")
	public float apparentTemperatureHigh;
	@JsonProperty("apparentTemperatureHighTime")
	public int apparentTemperatureHighTime;
	@JsonProperty("apparentTemperatureLow")
	public float apparentTemperatureLow;
	@JsonProperty("apparentTemperatureLowTime")
	public int apparentTemperatureLowTime;
	@JsonProperty("dewPoint")
	public float dewPoint;
	@JsonProperty("humidity")
	public float humidity;
	@JsonProperty("pressure")
	public float pressure;
	@JsonProperty("windSpeed")
	public float windSpeed;
	@JsonProperty("windGust")
	public float windGust;
	@JsonProperty("windGustTime")
	public int windGustTime;
	@JsonProperty("windBearing")
	public int windBearing;
	@JsonProperty("cloudCover")
	public float cloudCover;
	@JsonProperty("uvIndex")
	public int uvIndex;
	@JsonProperty("uvIndexTime")
	public int uvIndexTime;
	@JsonProperty("visibility")
	public int visibility;
	@JsonProperty("ozone")
	public float ozone;
	@JsonProperty("temperatureMin")
	public float temperatureMin;
	@JsonProperty("temperatureMinTime")
	public int temperatureMinTime;
	@JsonProperty("temperatureMax")
	public float temperatureMax;
	@JsonProperty("temperatureMaxTime")
	public int temperatureMaxTime;
	@JsonProperty("apparentTemperatureMin")
	public float apparentTemperatureMin;
	@JsonProperty("apparentTemperatureMinTime")
	public int apparentTemperatureMinTime;
	@JsonProperty("apparentTemperatureMax")
	public float apparentTemperatureMax;
	@JsonProperty("apparentTemperatureMaxTime")
	public int apparentTemperatureMaxTime;
}
