package sbs.spring.weather.service;

import java.util.List;

import org.springframework.stereotype.Service;

import sbs.spring.weather.model.CountryWeather;

@Service
public interface WeatherService {
	List<CountryWeather> getWeather();
}
