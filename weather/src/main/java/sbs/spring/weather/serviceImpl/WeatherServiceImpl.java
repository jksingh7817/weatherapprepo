package sbs.spring.weather.serviceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
/*import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
*/import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import sbs.spring.weather.model.CountryWeather;
//import sbs.spring.weather.repository.LocationDataRepository;
import sbs.spring.weather.service.WeatherService;

@Component
public class WeatherServiceImpl implements WeatherService{
	
	@Value("${app.darkSkyKey}")
    private String darkSkyKey;
	
	/*
	 * @Autowired MongoTemplate mongoTemplate;
	 */
	
	/*
	 * @Autowired LocationDataRepository locationDataRepository;
	 */
	
	@Override
	public java.util.List<CountryWeather> getWeather() {
		WeatherServiceImpl impl= new WeatherServiceImpl();
		java.util.List<CountryWeather> countryWeatherList = new ArrayList<CountryWeather>();
		java.util.List<String> locationList = impl.getLocatioinList();
		
		//System.out.println(locationDataRepository.existsById("1"));
		//locationDataRepository.findAll().forEach(data -> System.out.println(data));
		//impl.addData();
		
		
		
		RestTemplate restTemp = new RestTemplate();
		for (int i=0;i<locationList.size();i++) {
			countryWeatherList.add(restTemp.getForObject(darkSkyKey+locationList.get(i), CountryWeather.class));
		}
		
		return countryWeatherList;
	}
	
	private java.util.List<String> getLocatioinList(){
		java.util.List<String> locatioinList = new ArrayList<String>();
		
		locatioinList.add("37.2872,121.9500");
		locatioinList.add("41.2565,95.9345");
		locatioinList.add("30.2672,97.7431");
		locatioinList.add("42.8048,140.6874");
		locatioinList.add("34.6851,135.8048");
		locatioinList.add("6.2088,106.8456");
		
		return locatioinList;
		
	}
	
	/*
	 * public void addData() { System.out.println("Adding sample data");
	 * locationDataRepository.save(new LocationData("1","", "New York","",""));
	 * 
	 * 
	 * }
	 */

}
